$( document ).ready(function() {

	//main functions
	function parallaxScroll(){
		$('body').scrollTop(0);
		
		$(window).bind('scroll',function(e){

			scrolled = $(window).scrollTop();

			$('.parallax__layer--1').css('transform', 'translateY(' + (0-(scrolled*.1)) + 'px)' );
			$('.parallax__layer--2').css('transform', 'translateY(' + (0-(scrolled*.5)) + 'px)' );
			$('.parallax__layer--3').css('transform', 'translateY(' + (0-(scrolled*2)) +'px)' );

		});
	}

	//pages:
	//index.html
	if ( $('.basket').length > 0 ) {
		parallaxScroll();

	}

	if ( $('.index').length > 0 ) {

		//mouse effect
		var scene = document.getElementById('scene');
		parallax = new Parallax(scene, {
			frictionX: 0.1,
			frictionY: 0.1
		});

		//slider
		$(".owl").owlCarousel({
			items: 1,
			nav: true,
			navText: ['', ''],
			dots: false,
			navClass: ['button button--prev', 'button button--next'],
			responsive: {

				0 : {

					margin: 100
				},

				1279 : {
					margin: 50
				}
			}

		});
	}

	//components:
	//menu
	if (  $('aside.aside').length > 0 ) {

		var burger = $('.hamburger');
		var menu = $('.aside');

		menuToggle();
		menuResize();
		menu.mCustomScrollbar();


		function menuToggle() {

			burger.on('click', function() {

				if ( !burger.hasClass('is-active') ) {

					$(this).addClass('is-active');
					menu.addClass('is-active');
				} else {
					$(this).removeClass('is-active');
					menu.removeClass('is-active');
				}

				return false;
			});
		}

		function menuResize() {

			$(window).on('resize', function() {

				if ($(window).width() > 1023) {
					burger.removeClass('is-active');
					menu.removeClass('is-active');
				}
			});
		}

		function menuScrollfix() {
			
			if ( $(window).width() > 1023 && $(window).height() < 1024 ) {
				menu.mCustomScrollbar();
			} else {
				menu.mCustomScrollbar("destroy")
			}

			$(window).on('resize', function() {
				if ( $(window).width() > 1023 && $(window).height() < 1024 ) {
					menu.mCustomScrollbar();
				}  else {
					menu.mCustomScrollbar("destroy")
				}
			})
		}
	}

	//fly 
	if( $('.fly').length > 0 ) {

		var fly = $('.fly');
		var flyContent = $('.fly__content');
		var opener = $('.fly-opener');
		var closer = $('.fly-closer');
		var remove = $('.fly__item-remove');
		var items = $('.fly__item').length;


		flyContent.mCustomScrollbar()

		opener.on('click', function(e) {

			if( $(window).width() > 1023 && items > 0) {
				fly.addClass('is-shown');
				return false;

			} else {
				return true;
			}
		});

		closer.on('click', function() {
			fly.removeClass('is-shown');

			return false;
		});


		remove.on('click', function() {
			$(this).closest('.fly__item').fadeOut(400, function() {
				$(this).remove();
				items--;
			})
		});
		
	}

	//counter
	if ( $('.counter').length > 0 ) {

		var counter = $('.counter');
		var plus = $('.counter__plus');
		var minus = $('.counter__minus');
		var count;
		var newCount;

		$('input').on('focus', function() {
			var tmpStr = $(this).val();
			$(this).val('');
			$(this).val(tmpStr);
		});

		plus.on('click', function() {

			count = $(this).siblings('.counter__result').find('.counter__input').val();

			newCount =  +count + 1;

			if ( newCount > 0 ) {

				$(this).siblings('.counter__result').find('.counter__input').val(newCount);

			}

			return false;
		});

		minus.on('click', function() {

			count = $(this).siblings('.counter__result').find('.counter__input').val();
			newCount =  +count - 1;

			if ( newCount > 0 ) {

				$(this).siblings('.counter__result').find('.counter__input').val(newCount);			
			}

			return false;
		});
	}
});

